# ascii-color-chooser

A terminal color chooser and navigator.

This script helps to answer those questions:

- Which color code correspond to which color?
- Given a color (either term code or rgb), what is the rgb values of a darker/lighter, more saturate / less saturate one?

## Starting

### Prequisite

- ```bash(1)```
- ```bc(1)```
- A color terminal!

### Installation

Git clone this repository.

Either copy the script in your PATH, make a symlink, or an alias.

## Using

### Overview

```
	# Show colors in row
	ascii-color-chooser [-l <n> ] [ -i ]
	# Show them by section
	ascii-color-chooser -s [ -i ]
	# Color tuner
	ascii-color-chooser -c <xterm|r:g:b|#rgb>
	# Print string in rainbow mode
	ascii-color-chooser -r <string>
	# Or fade to black
	ascii-color-chooser -f <string>
```

### In depth

#### Display

Show the 256 colors, ordered by number:

```ascii-color-chooser```

![Show linear](/doc/acc-block.png)

Show the 256 colors, ordered by number but splitted by sections (standard, high intensity, the 6x6x6 cube, and gray scale)

```ascii-color-chooser -s```

![Show by section](/doc/acc-sections.png)

You can navigate through sections (interactive mode):

```
ascii-color-chooser -i
ascii-color-chooser -si
```

![Navigate by section](/doc/acc-sections-interactive.png)

ASCII code is used as is:
```
$ echo -e "\e[38;5;<code>m"		# set foreground color
$ echo -e "\e[48;5;<code>m"		# set background color
```

#### Color navigation

You can also "navigate" from a color. Either specifying xterm code, xterm name, or rgb values.

```
ascii-color-chooser -c 227
ascii-color-chooser -c 255:255:98
ascii-color-chooser -c '#95d5B2'
ascii-color-chooser -c BrightRed
ascii-color-chooser -c BlUe
```

Then use:

- left/right to change hue (0-360°, wrapping) (think of walking around on the hsl circle)
- up/down to change saturation (0-100%, no wrapping) (getting farer/closer from the center of the circle)
- +/- to change luminosity (0-100%, no wrapping)
- 'r' to reset to the starting color
- 'q' to quit

HSL code is shown above, rgb code is shown below.

![Color tuner](/doc/acc-tuner.png)

Use the rgb code as is:

```
$ echo -e "\e[38;2;<r>;<g>;<b>m"	# foreground
$ echo -e "\e[48;2;<r>;<g>;<b>m"	# background
```

#### Rainbow

Display a string as rainbow:

```ascii-color-chooser -r "Lorem ipsum dolor sit amet, consectetur adipiscing elit"```

![Lorem ipsum](/doc/loremipsum.png)

You can also use ```figlet(6)``` output:

```ascii-color-chooser -r "$(figlet -f big 'The Rainbow')"```

![Use figlet](/doc/figlet.png)

And make some fancy stuff with [spwgen](https://github.com/Korsani/shaped-pwgen)

```ascii-color-chooser -r "$(spwgen -i angel.png 75)"```

![With spwgen](/doc/rainbow-angel.png)


Please note that:

- Starting color is randomly choosen unless you specify ```-o hue_start=<n>```
- Colors will be used once
- Ending color will be the starting one (or try to)

#### Fade to black

Same usage as rainbow, but starting color is always bright white.

```ascii-color-chooser -f "Lorem ipsum dolor sit amet, consectetur adipiscing elit"```

![Fade lorem ipsum](/doc/loremipsum-fade.png)

## Known limitations

- Don't work on legacy MacOS terminal. I don't know why... But work with iTerm2.
- hsl<=>rgb conversion is not "stable": hsl to rgb to hsl does not result in the first hsl.

## Authors

Me :)

## License

GPL v3
