#!/usr/bin/env bash
function succeed() {
        echo 'Test ok'
}
echo 'q' |	./ascii-color-chooser -c 255:255:98	>	/dev/null		&& succeed
echo 'q' |	./ascii-color-chooser -c REd		>	/dev/null		&& succeed
			./ascii-color-chooser -c plop 		2>	/dev/null		|| succeed
			./ascii-color-chooser -s			>	/dev/null		&& succeed
			./ascii-color-chooser 				>	/dev/null		&& succeed
